# Changelog
All notable changes to this repository will be documented in this file.

## [0.0.1] - 2019-07-10
### Added
- Basic files of any repository as _README.md_ _CHANGELOG.md_ _.gitignore_ and others.
- _lICENSE_ file with the legal information concerning this repository.

## [0.0.2] - 2021-03-25
### Fixed
- Fix the links to new domain in _README.md_ _CHANGELOG.md_ _.gitignore_ and other files.
### Changed
- new mixed-use covers added

## [0.0.3] - 2021-03-27
### Added
- Folder with the source files of the fonts used in all the applications