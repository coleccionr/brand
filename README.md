<div align="center">
![\\\\cR coleccióón Rorikon](Resources/Logotype/Bitmaps/logotype.png)
</div>

***
 
 # Brand #

Below are the guidelines, rules, recommendations, elements and supplies; as well source code, 
files and documents necessary to make a correct and effective application of the colección Rorikon Brand.

Don't forget to visit the _About_ and _Brand Use_ sections in our website to better understand where 
the graphic concept comes from, use of images of some people and what each element means or indicates.

[About \\\\cR](https://www.coleccionr.net/about)  &emsp;|&emsp;  [Brand Use](https://www.coleccionr.net/about/brand)

## Table of Contents

- [Brand](#brand)
  - [Table of Contents](#table-of-contents)
  - [Usage](#usage)
    - [Steps to use this repository](#steps-to-use-this-repository)
  - [Clone / Download the Repository](#clone--download-the-repository)
      - [Clone the Repository](#clone-the-repository)
      - [Download](#download)
  - [Changelog](#changelog)
  - [Support](#support)
  - [License](#license)

## Usage
This repository is intended to house all the necessary resources and guidelines to make the correct 
use of the colors, fonts, designs, logos, isotypes, symbols, and other elements of the colección Rorikon brand, positioning them in the different communicative applications necessary to strengthen the commitment 
and the sense of belonging inherent in the brand.

To that extent, those people who make use of elements, works and references identified with the colección Rorikon Brand should be empowered of all the resources available in this repository and develop in an accurate and unequivocal way the brand image `without no exception`.

### Steps to use this repository

1.   First of all, check the [Brand Manual]() 
and the different possibilities of graphic application in the described one.  

2.   Then define that you need to use the brand; only the logo in horizontal version in bitmap, or require the entire resource package including the vector source file, and based on your decision define whether to 
[clone or download](#clone-download-the-repository) the entire repository or only what you need.  

3.   Finally, check that all graphic elements used comply with the guidelines established in the _Brand Manual_ and have a harmonious relationship with the surrounding elements.

Don't forget that if you have doubts about how to use or apply any of the elements of the brand you can write me to the contact information available in [Support](#support).


## Clone / Download the Repository

You can choose any of the options listed below.

#### Clone the Repository

If you think that you will need several and / or recurrently the resources of the brand, you also have a basic management 
of git you can clone my repository and be up to date with the latest changes and additions to the repository later; simply 
have installed [git](https://git-scm.com/) on your computer, open a terminal in the folder where you want to store the 
repository and execute the following command.

```
git clone https://gitlab.com/coleccionr/brand.git

```
Remember to avoid editing the repository files directly, instead you can make a copy out of the repository folder and edit 
it with confidence to avoid conflicts when you want to refresh (pull request) future possible changes.

#### Download

If you do not have git handling, you can simply download a compressed copy of the repository that includes all the files; 
or failing that, navigate through the different repository directories and access the specific resource required.

*  [Download .zip](https://gitlab.com/coleccionr/brand/-/archive/master/brand-master.zip)
*  [Download .tar.gz](https://gitlab.com/coleccionr/brand/-/archive/master/brand-master.tar.gz)

## Changelog

If you have cloned this repository before or have a downloaded copy, do not forget to check the [changelog](CHANGELOG.md) to make sure you 
are working with the _latest changes uploaded_ to the repository

## Support

If you have any doubts or suggestions when using the elements provided in this repository, do not hesitate to contact me 
through the different means and networks available below, I will be attentive to answer any questions in the shortest 
possible time

[www.coleccionr.net](https://www.coleccionr.net)  

[support@coleccionr.net](mailto:support@coleccionr.net) 

Additionally you can contact me at

[www.kaoi97.net](https://www.kaoi97.net)  

[leonardo@kaoi97.net](mailto:leonardo@kaoi97.net)  

[(+57)314 4818811](https://t.me/kAoi97)  


## License

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
<img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>
<br />
<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">colección Rorikon Brand</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.kaoi97.net/" property="cc:attributionName" rel="cc:attributionURL">kAoi97</a>
is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.<br /><br />
Permissions beyond the scope of this license may be available at <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.coleccionr.net/about/legal" rel="cc:morePermissions">https://www.coleccionr.net/about/legal</a>.
  
